ARG BUILD_ARG_APP_USER_ID=1048576
ARG BUILD_ARG_LDAP_ROOT="dc=databus,dc=localhost"
ARG BUILD_ARG_KDC_REALM="DATABUS.LOCALHOST"
ARG BUILD_ARG_ROOT_PASSWORD="adminpassword"

FROM busybox as auth
ARG BUILD_ARG_APP_USER_ID
COPY ./images/auth/stages/auth/ /
RUN /build.d/bin/build.sh
RUN /build.d/bin/post-build.sh
USER ${BUILD_ARG_APP_USER_ID}
CMD ["sleep", "infinity"]

FROM alpine as auth-krb5conf
ARG BUILD_ARG_LDAP_ROOT
ARG BUILD_ARG_KDC_REALM
COPY ./images/auth/stages/auth-krb5conf/ /
RUN /build.d/bin/build.sh
RUN /build.d/bin/post-build.sh

FROM alpine as auth-kdc
ARG BUILD_ARG_APP_USER_ID
COPY ./images/auth/stages/auth-kdc/ /
RUN /build.d/bin/build.sh
RUN /build.d/bin/post-build.sh
COPY --from=auth-krb5conf /etc/krb5.conf /etc/
USER ${BUILD_ARG_APP_USER_ID}
ENTRYPOINT ["./entrypoint.sh"]
CMD ["krb5kdc"]

FROM bitnami/openldap as auth-openldap
ARG BUILD_ARG_APP_USER_ID
ARG BUILD_ARG_LDAP_ROOT
USER root
COPY ./images/auth/stages/auth-openldap/ /
RUN /build.d/bin/build.sh
RUN /build.d/bin/post-build.sh
ENV LDAP_ROOT=${BUILD_ARG_LDAP_ROOT}
USER ${BUILD_ARG_APP_USER_ID}
