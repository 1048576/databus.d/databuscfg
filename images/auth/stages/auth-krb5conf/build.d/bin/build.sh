#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "sed -i 's|{{ BUILD_ARG_LDAP_ROOT }}|${BUILD_ARG_LDAP_ROOT}|g' /etc/krb5.conf"
_sh "sed -i 's|{{ BUILD_ARG_KDC_REALM }}|${BUILD_ARG_KDC_REALM}|g' /etc/krb5.conf"

# Post install
_sh "rm $0"
