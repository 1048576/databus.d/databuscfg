#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

exec /usr/bin/java -jar /app/databus-worker-http.jar $@
