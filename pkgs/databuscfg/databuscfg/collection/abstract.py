from typing import Any
from typing import Generic
from typing import Protocol
from typing import TypeVar

from pygentree.abstract import Tree


class Item(Protocol):
    def __str__(self) -> str:
        ...

    def __lt__(self, __x: Any) -> bool:
        ...


ItemType = TypeVar(
    name="ItemType",
    bound=Item,
    covariant=True
)


class ItemProcessor(Generic[ItemType]):
    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> ItemType:
        ...
