from __future__ import annotations

from typing import Dict
from typing import Iterable
from typing import List
from typing import Tuple

import databuscfg.collection.utils

from databuscfg.configuration.abstract import Configuration
from databuscfg.loader.abstract import Loader
from databuscfg.processor.collection.impl import CollectionProcessorImpl
from databuscfg.processor.entity.impl import EntityProcessorImpl
from databuscfg.processor.node.impl import NodeProcessorImpl
from databuscfg.processor.route.impl import RouteProcessorImpl
from databuscfg.processor.system.impl import SystemProcessor
from databuscfg.profile.collection.abstract import CollectionProfile
from databuscfg.profile.entity.abstract import EntityProfile
from databuscfg.profile.entity.abstract import EntityProfileRoute
from databuscfg.profile.entity.abstract import EntityProfileRouteKind
from databuscfg.profile.route.abstract import RouteProfile
from databuscfg.profile.system.abstract import SystemProfile
from databuscfg.validator.collection.impl import CollectionValidatorImpl
from databuscfg.validator.node.impl import NodeValidatorImpl
from databuscfg.validator.route.impl import RouteValidatorImpl
from databuscfg.validator.system.impl import SystemValidatorImpl
from pygentree.abstract import Tree


class ConfigurationImpl(Configuration):
    @staticmethod
    def load(loader: Loader) -> ConfigurationImpl:
        system_tree_it = loader.load_collection(
            collection_id="systems",
            err_msg="Cannot load systems"
        )

        node_tree_it = loader.load_collection(
            collection_id="nodes",
            err_msg="Cannot load nodes"
        )

        route_tree_it = loader.load_collection(
            collection_id="routes",
            err_msg="Cannot load routes"
        )

        collection_tree_it = loader.load_collection(
            collection_id="collections",
            err_msg="Cannot load collections"
        )

        environment_tree_it = loader.load_collection(
            collection_id="environments",
            err_msg="Cannot load environments"
        )

        entity_tree_it = loader.load_collection(
            collection_id="entities",
            err_msg="Cannot load entities"
        )

        return ConfigurationImpl.create(
            system_tree_it=system_tree_it,
            node_tree_it=node_tree_it,
            route_tree_it=route_tree_it,
            collection_tree_it=collection_tree_it,
            environment_tree_it=environment_tree_it,
            entity_tree_it=entity_tree_it
        )

    @staticmethod
    def create(
        system_tree_it: Iterable[Tuple[str, Tree]],
        node_tree_it: Iterable[Tuple[str, Tree]],
        route_tree_it: Iterable[Tuple[str, Tree]],
        collection_tree_it: Iterable[Tuple[str, Tree]],
        environment_tree_it: Iterable[Tuple[str, Tree]],
        entity_tree_it: Iterable[Tuple[str, Tree]]
    ) -> ConfigurationImpl:
        systems = databuscfg.collection.utils.process(
            collection_name="Systems",
            item_name="System",
            tree_items=system_tree_it,
            processor=SystemProcessor()
        )

        nodes = databuscfg.collection.utils.process(
            collection_name="Nodes",
            item_name="Node",
            tree_items=node_tree_it,
            processor=NodeProcessorImpl()
        )

        system_validator = SystemValidatorImpl(set(systems))
        node_validator = NodeValidatorImpl(set(nodes))

        route_processor = RouteProcessorImpl(
            system_validator=system_validator,
            node_validator=node_validator
        )
        routes = databuscfg.collection.utils.process(
            collection_name="Routes",
            item_name="Route",
            tree_items=route_tree_it,
            processor=route_processor
        )

        route_validator = RouteValidatorImpl(set(routes))

        collection_processor = CollectionProcessorImpl(
            route_validator=route_validator
        )
        collections = databuscfg.collection.utils.process(
            collection_name="Collections",
            item_name="Collection",
            tree_items=collection_tree_it,
            processor=collection_processor
        )

        collection_validator = CollectionValidatorImpl(set(collections))

        entity_processor = EntityProcessorImpl(
            route_validator=route_validator,
            collection_validator=collection_validator
        )
        entities = databuscfg.collection.utils.process(
            collection_name="Entities",
            item_name="Entity",
            tree_items=entity_tree_it,
            processor=entity_processor
        )

        # environment_processor = EnvironmentProcessorImpl(system_validator)
        # databuscfg.collection.utils.process(
        #     collection_name="Environments",
        #     item_name="Environment",
        #     tree_items=environment_tree_it,
        #     processor=environment_processor
        # )

        return ConfigurationImpl(
            systems=systems,
            routes=routes,
            collections=collections,
            entities=entities
        )

    _systems: Dict[str, SystemProfile]
    _routes: Dict[str, RouteProfile]
    _collections: Dict[str, CollectionProfile]
    _entities: Dict[str, EntityProfile]

    def __init__(
        self,
        systems: Dict[str, SystemProfile],
        routes: Dict[str, RouteProfile],
        collections: Dict[str, CollectionProfile],
        entities: Dict[str, EntityProfile]
    ) -> None:
        self._systems = systems
        self._routes = routes
        self._collections = collections
        self._entities = entities

    def systems(self) -> Dict[str, SystemProfile]:
        return self._systems

    def routes(self) -> Dict[str, RouteProfile]:
        return self._routes

    def collections(self) -> Dict[str, CollectionProfile]:
        return self._collections

    def entities(self) -> Dict[str, EntityProfile]:
        return self._entities

    def entity_routes(self, entity: EntityProfile) -> List[RouteProfile]:
        EPRK = EntityProfileRouteKind

        calls = {
            EPRK.ROUTE: self._entity_routes_kind_route,
            EPRK.COLLECTION: self._entity_routes_kind_collection
        }

        routes: List[RouteProfile] = []
        for entity_route in entity.routes():
            call = calls[entity_route.kind()]
            routes.extend(call(entity_route))

        return routes

    def _entity_routes_kind_route(
        self,
        entity_route: EntityProfileRoute
    ) -> List[RouteProfile]:
        return [
            self._routes[entity_route.unique_id()]
        ]

    def _entity_routes_kind_collection(
        self,
        entity_route: EntityProfileRoute
    ) -> List[RouteProfile]:
        collection = self._collections[entity_route.unique_id()]

        routes: List[RouteProfile] = []
        for collection_route in collection.routes():
            routes.append(self._routes[collection_route.unique_id()])

        return routes
