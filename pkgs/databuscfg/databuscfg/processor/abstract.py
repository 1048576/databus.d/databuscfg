from typing import Any
from typing import Dict
from typing import Generic
from typing import Iterable
from typing import Protocol
from typing import Tuple
from typing import TypeVar

from pygentree.abstract import Tree


class Item(Protocol):
    def __str__(self) -> str:
        raise NotImplementedError()

    def __lt__(self, __x: Any) -> bool:
        raise NotImplementedError()


ItemType = TypeVar(
    name="ItemType",
    bound=Item,
    covariant=True
)


class ItemProcessor(Protocol, Generic[ItemType]):
    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> ItemType:
        raise NotImplementedError()


class ItemsProcessor(Generic[ItemType]):
    def process(
        self,
        items: Iterable[Tuple[str, Tree]]
    ) -> Dict[str, ItemType]:
        raise NotImplementedError()
