from __future__ import annotations

from typing import Dict
from typing import Tuple

from databuscfg.collection.abstract import ItemProcessor
from databuscfg.profile.environment.abstract import EnvironmentProfile
from databuscfg.profile.environment.impl import EnvironmentProfileImpl
from databuscfg.profile.node.abstract import NodeId
from databuscfg.profile.system.abstract import SystemId
from databuscfg.profile.vhost.abstract import VhostId
from databuscfg.profile.zone.abstract import VhostProfile
from databuscfg.profile.zone.abstract import ZoneId
from databuscfg.profile.zone.abstract import ZoneProfile
from databuscfg.validator.system.impl import SystemValidatorImpl
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException

Items = Dict[Tuple[SystemId, NodeId], Tuple[ZoneId, VhostId]]


class EnvironmentProcessorImpl(ItemProcessor[EnvironmentProfile]):
    _system_validator: SystemValidatorImpl

    def __init__(
        self,
        system_validator: SystemValidatorImpl
    ) -> None:
        self._system_validator = system_validator

    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> EnvironmentProfile:
        try:
            environment = EnvironmentProfileImpl.create(tree)
            producers: Items = {}
            consumers: Items = {}

            for zone_id, zone in sorted(environment.zones().items()):
                self._process_zone(
                    producers=producers,
                    consumers=consumers,
                    zone_id=zone_id,
                    zone=zone
                )

            return environment
        except ExpectedException as e:
            raise e.wrap(
                title="Environment [{}]".format(
                    unique_id
                )
            )

    def _process_zone(
        self,
        producers: Items,
        consumers: Items,
        zone_id: ZoneId,
        zone: ZoneProfile
    ) -> None:
        try:
            for vhost_id, vhost in sorted(zone.vhosts().items()):
                try:
                    self._process_vhost(
                        producers=producers,
                        consumers=consumers,
                        zone_id=zone_id,
                        vhost_id=vhost_id,
                        vhost=vhost
                    )
                except ExpectedException as e:
                    raise e.wrap(
                        title="Vhost [{}]".format(
                            vhost_id
                        )
                    )
        except ExpectedException as e:
            raise e.wrap(
                title="Zone [{}]".format(
                    zone_id
                )
            )

    def _process_vhost(
        self,
        producers: Items,
        consumers: Items,
        zone_id: ZoneId,
        vhost_id: VhostId,
        vhost: VhostProfile
    ) -> None:
        for producer_id, producer in sorted(vhost.producers().items()):
            try:
                self._system_validator.validate(producer_id)

                for node_id in producer.nodes():
                    try:
                        self._process_node(
                            items=producers,
                            zone_id=zone_id,
                            vhost_id=vhost_id,
                            system_id=producer_id,
                            node_id=node_id
                        )
                    except ExpectedException as e:
                        raise e.wrap(
                            title="Node [{}]".format(
                                node_id
                            )
                        )
            except ExpectedException as e:
                raise e.wrap(
                    title="Producer [{}]".format(
                        producer_id
                    )
                )

        for consumer_id, consumer in sorted(vhost.consumers().items()):
            try:
                self._system_validator.validate(consumer_id)

                for node_id in consumer.nodes():
                    try:
                        self._process_node(
                            items=consumers,
                            zone_id=zone_id,
                            vhost_id=vhost_id,
                            system_id=consumer_id,
                            node_id=node_id
                        )
                    except ExpectedException as e:
                        raise e.wrap(
                            title="Node [{}]".format(
                                node_id
                            )
                        )
            except ExpectedException as e:
                raise e.wrap(
                    title="Consumer [{}]".format(
                        consumer_id
                    )
                )

    def _process_node(
        self,
        items: Items,
        zone_id: ZoneId,
        vhost_id: VhostId,
        system_id: SystemId,
        node_id: NodeId
    ) -> None:
        key = (system_id, node_id)

        item = items.get(key)
        if (item is not None):
            raise ExpectedException(
                msg="Node already defined zones[{}].vhosts[{}]".format(
                    item[0],
                    item[1]
                )
            )
        else:
            new_item = (zone_id, vhost_id)
            items[key] = new_item
