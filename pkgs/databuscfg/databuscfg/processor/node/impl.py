from __future__ import annotations

from databuscfg.collection.abstract import ItemProcessor
from databuscfg.profile.node.abstract import NodeProfile
from databuscfg.profile.node.impl import NodeProfileImpl
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


class NodeProcessorImpl(ItemProcessor[NodeProfile]):
    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> NodeProfile:
        try:
            node = NodeProfileImpl.create(tree)

            return node
        except ExpectedException as e:
            raise e.wrap(
                title="Node [{}]".format(
                    unique_id
                )
            )
