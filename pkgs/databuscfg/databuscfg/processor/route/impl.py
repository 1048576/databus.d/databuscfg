from __future__ import annotations

from typing import Set
from typing import Tuple

from databuscfg.collection.abstract import ItemProcessor
from databuscfg.profile.route.abstract import RouteProfile
from databuscfg.profile.route.abstract import RouteRuleProfile
from databuscfg.profile.route.impl import RouteProfileImpl
from databuscfg.validator.node.impl import NodeValidatorImpl
from databuscfg.validator.system.impl import SystemValidatorImpl
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


class RouteProcessorImpl(ItemProcessor[RouteProfile]):
    _system_validator: SystemValidatorImpl
    _node_validator: NodeValidatorImpl

    def __init__(
        self,
        system_validator: SystemValidatorImpl,
        node_validator: NodeValidatorImpl
    ) -> None:
        self._system_validator = system_validator
        self._node_validator = node_validator

    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> RouteProfile:
        try:
            route = RouteProfileImpl.create(unique_id, tree)

            if (route.title() == ""):
                raise ExpectedException("Title is required")

            try:
                self._system_validator.validate(route.producer())
            except ExpectedException as e:
                raise e.wrap(
                    title="Producer [{}]".format(
                        route.producer()
                    )
                )

            try:
                self._system_validator.validate(route.consumer())
            except ExpectedException as e:
                raise e.wrap(
                    title="Consumer [{}]".format(
                        route.consumer()
                    )
                )

            self._process_rules(route)

            return route
        except ExpectedException as e:
            raise e.wrap(
                title="Route [{}]".format(
                    unique_id
                )
            )

    def _process_rules(self, route: RouteProfile) -> None:
        rules: Set[Tuple[str, str, str]] = set()
        for rule in route.rules():
            self._process_rule(rules, rule)

    def _process_rule(
        self,
        rule_set: Set[Tuple[str, str, str]],
        rule: RouteRuleProfile
    ) -> None:
        try:
            self._node_validator.validate(rule.producer())
        except ExpectedException as e:
            raise e.wrap(
                title="Producer [{}]".format(
                    rule.producer()
                )
            )

        try:
            self._node_validator.validate(rule.consumer())
        except ExpectedException as e:
            raise e.wrap(
                title="Consumer [{}]".format(
                    rule.consumer()
                )
            )

        rule_set_entry = (rule.producer(), rule.consumer(), rule.label())
        if (rule_set_entry in rule_set):
            raise ExpectedException(
                msg="Rule [{}]->[{}]:[{}] already defined".format(
                    rule.producer(),
                    rule.consumer(),
                    rule.label()
                )
            )
        else:
            rule_set.add(rule_set_entry)
