from __future__ import annotations

from databuscfg.collection.abstract import ItemProcessor
from databuscfg.profile.system.abstract import SystemProfile
from databuscfg.profile.system.impl import SystemProfileImpl
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


class SystemProcessor(ItemProcessor[SystemProfile]):
    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> SystemProfile:
        try:
            system = SystemProfileImpl.create(unique_id, tree)

            return system
        except ExpectedException as e:
            raise e.wrap(
                title="System [{}]".format(
                    unique_id
                )
            )
