from __future__ import annotations

from enum import Enum
from typing import List
from typing import NewType

from databuscfg.profile.abstract import Profile

EntityId = NewType("EntityId", str)


class EntityProfileRouteKind(Enum):
    ROUTE = ("route", "Route")
    COLLECTION = ("collection", "Collection")

    def key(self) -> str:
        return self.value[0]

    def title(self) -> str:
        return self.value[1]


class EntityProfileRoute:
    def kind(self) -> EntityProfileRouteKind:
        ...

    def unique_id(self) -> str:
        ...


class EntityProfile(Profile[EntityId]):
    def title(self) -> str:
        ...

    def wiki(self) -> str:
        ...

    def refs(self) -> List[str]:
        ...

    def routes(self) -> List[EntityProfileRoute]:
        ...
