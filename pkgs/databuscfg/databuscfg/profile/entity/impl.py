from __future__ import annotations

from typing import List

from databuscfg.profile.abstract import Profile
from databuscfg.profile.entity.abstract import EntityId
from databuscfg.profile.entity.abstract import EntityProfile
from databuscfg.profile.entity.abstract import EntityProfileRoute
from databuscfg.profile.entity.abstract import EntityProfileRouteKind
from pygentree.abstract import Tree
from pygentree.exception import TreeNodeNotFoundException
from pygentype.exception import UnreachableStateException


class EntityProfileRouteImpl(EntityProfileRoute):
    @staticmethod
    def create(tree: Tree) -> EntityProfileRoute:
        with (tree):
            for kind in EntityProfileRouteKind:
                try:
                    return EntityProfileRouteImpl(
                        kind=kind,
                        unique_id=tree.detach_text_node(kind.key())
                    )
                except TreeNodeNotFoundException:
                    pass

        raise UnreachableStateException()

    _kind: EntityProfileRouteKind
    _unique_id: str

    def __init__(self, kind: EntityProfileRouteKind, unique_id: str) -> None:
        self._kind = kind
        self._unique_id = unique_id

    def kind(self) -> EntityProfileRouteKind:
        return self._kind

    def unique_id(self) -> str:
        return self._unique_id


class EntityProfileImpl(EntityProfile):
    @staticmethod
    def create(unique_id: str, tree: Tree) -> EntityProfile:
        with (tree):
            return EntityProfileImpl(
                unique_id=EntityId(unique_id),
                title=tree.detach_text_node("title"),
                wiki=tree.detach_text_node("wiki"),
                refs=tree.detach_text_list_node("refs"),
                routes=list(
                    map(
                        EntityProfileRouteImpl.create,
                        tree.detach_tree_list_node("routes")
                    )
                )
            )

    _unique_id: EntityId
    _title: str
    _wiki: str
    _refs: List[str]
    _routes: List[EntityProfileRoute]

    def __init__(
        self,
        unique_id: EntityId,
        title: str,
        wiki: str,
        refs: List[str],
        routes: List[EntityProfileRoute]
    ) -> None:
        self._unique_id = unique_id
        self._title = title
        self._wiki = wiki
        self._refs = refs
        self._routes = routes

    def unique_id(self) -> EntityId:
        return self._unique_id

    def title(self) -> str:
        return self._title

    def wiki(self) -> str:
        return self._wiki

    def refs(self) -> List[str]:
        return self._refs

    def routes(self) -> List[EntityProfileRoute]:
        return self._routes

    def __lt__(self, other: Profile[EntityId]) -> bool:
        return (self.unique_id() < other.unique_id())
