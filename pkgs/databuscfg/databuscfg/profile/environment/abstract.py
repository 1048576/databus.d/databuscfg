from typing import Dict
from typing import NewType

from databuscfg.profile.abstract import Profile
from databuscfg.profile.zone.abstract import ZoneId
from databuscfg.profile.zone.abstract import ZoneProfile

EnvironmentId = NewType("EnvironmentId", str)


class EnvironmentProfile(Profile[EnvironmentId]):
    def zones(self) -> Dict[ZoneId, ZoneProfile]:
        ...
