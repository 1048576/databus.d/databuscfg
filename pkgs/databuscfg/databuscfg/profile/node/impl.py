from __future__ import annotations

from databuscfg.profile.node.abstract import NodeProfile
from pygentree.abstract import Tree


class NodeProfileImpl(NodeProfile):
    @staticmethod
    def create(tree: Tree) -> NodeProfile:
        with (tree):
            return NodeProfileImpl(
                title=tree.detach_text_node("title")
            )

    _title: str

    def __init__(self, title: str) -> None:
        self._title = title
