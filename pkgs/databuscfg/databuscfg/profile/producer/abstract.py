from typing import List

from databuscfg.profile.node.abstract import NodeId


class ProducerProfile:
    def nodes(self) -> List[NodeId]:
        ...
