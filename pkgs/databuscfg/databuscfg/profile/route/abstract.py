from typing import List
from typing import NewType

from databuscfg.profile.abstract import Profile
from databuscfg.profile.node.abstract import NodeId
from databuscfg.profile.system.abstract import SystemId

RouteId = NewType("RouteId", str)


class RouteRuleProfile:
    def producer(self) -> NodeId:
        ...

    def consumer(self) -> NodeId:
        ...

    def label(self) -> str:
        ...


class RouteProfile(Profile[RouteId]):
    def title(self) -> str:
        ...

    def producer(self) -> SystemId:
        ...

    def consumer(self) -> SystemId:
        ...

    def exclusive(self) -> bool:
        ...

    def rules(self) -> List[RouteRuleProfile]:
        ...
