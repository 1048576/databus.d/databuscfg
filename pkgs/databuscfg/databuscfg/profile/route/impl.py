from typing import List

from databuscfg.profile.node.abstract import NodeId
from databuscfg.profile.route.abstract import RouteId
from databuscfg.profile.route.abstract import RouteProfile
from databuscfg.profile.route.abstract import RouteRuleProfile
from databuscfg.profile.system.abstract import SystemId
from pygentree.abstract import Tree


class RouteRuleProfileImpl(RouteRuleProfile):
    @staticmethod
    def create(tree: Tree) -> RouteRuleProfile:
        with (tree):
            return RouteRuleProfileImpl(
                producer=NodeId(tree.detach_text_node("producer")),
                consumer=NodeId(tree.detach_text_node("consumer")),
                label=tree.detach_text_node("label")
            )

    _producer: NodeId
    _consumer: NodeId
    _label: str

    def __init__(
        self,
        producer: NodeId,
        consumer: NodeId,
        label: str
    ) -> None:
        self._producer = producer
        self._consumer = consumer
        self._label = label

    def producer(self) -> NodeId:
        return self._producer

    def consumer(self) -> NodeId:
        return self._consumer

    def label(self) -> str:
        return self._label


class RouteProfileImpl(RouteProfile):
    @staticmethod
    def create(unique_id: str, tree: Tree) -> RouteProfile:
        with (tree):
            return RouteProfileImpl(
                unique_id=RouteId(unique_id),
                title=tree.detach_text_node("title"),
                producer=SystemId(tree.detach_text_node("producer")),
                consumer=SystemId(tree.detach_text_node("consumer")),
                exclusive=tree.detach_bool_node("exclusive"),
                rules=list(
                    map(
                        RouteRuleProfileImpl.create,
                        tree.detach_tree_list_node("rules")
                    )
                )
            )

    _unique_id: RouteId
    _title: str
    _producer: SystemId
    _consumer: SystemId
    _exclusive: bool
    _rules: List[RouteRuleProfile]

    def __init__(
        self,
        unique_id: RouteId,
        title: str,
        producer: SystemId,
        consumer: SystemId,
        exclusive: bool,
        rules: List[RouteRuleProfile]
    ) -> None:
        self._unique_id = unique_id
        self._title = title
        self._producer = producer
        self._consumer = consumer
        self._exclusive = exclusive
        self._rules = rules

    def unique_id(self) -> RouteId:
        return self._unique_id

    def title(self) -> str:
        return self._title

    def producer(self) -> SystemId:
        return self._producer

    def consumer(self) -> SystemId:
        return self._consumer

    def exclusive(self) -> bool:
        return self._exclusive

    def rules(self) -> List[RouteRuleProfile]:
        return self._rules
