from typing import NewType

from databuscfg.profile.abstract import Profile

SystemId = NewType("SystemId", str)


class SystemProfile(Profile[SystemId]):
    def title(self) -> str:
        raise NotImplementedError()
