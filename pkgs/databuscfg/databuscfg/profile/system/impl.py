from __future__ import annotations

from databuscfg.profile.system.abstract import SystemId
from databuscfg.profile.system.abstract import SystemProfile
from pygentree.abstract import Tree


class SystemProfileImpl(SystemProfile):
    @staticmethod
    def create(unique_id: str, tree: Tree) -> SystemProfileImpl:
        with (tree):
            return SystemProfileImpl(
                unique_id=SystemId(unique_id),
                title=tree.detach_text_node("title")
            )

    _unique_id: SystemId
    _title: str

    def __init__(self, unique_id: SystemId, title: str) -> None:
        self._unique_id = unique_id
        self._title = title

    def unique_id(self) -> SystemId:
        return self._unique_id

    def title(self) -> str:
        return self._title
