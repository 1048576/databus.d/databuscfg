from typing import Dict
from typing import NewType

from databuscfg.profile.consumer.abstract import ConsumerProfile
from databuscfg.profile.producer.abstract import ProducerProfile
from databuscfg.profile.system.abstract import SystemId

VhostId = NewType("VhostId", str)


class VhostProfile:
    def producers(self) -> Dict[SystemId, ProducerProfile]:
        ...

    def consumers(self) -> Dict[SystemId, ConsumerProfile]:
        ...
