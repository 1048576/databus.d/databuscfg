from typing import Dict

from databuscfg.profile.consumer.abstract import ConsumerProfile
from databuscfg.profile.producer.abstract import ProducerProfile
from databuscfg.profile.system.abstract import SystemId
from databuscfg.profile.vhost.abstract import VhostProfile
from pygentree.abstract import Tree


class VhostProfileImpl(VhostProfile):
    @staticmethod
    def create(tree: Tree) -> VhostProfile:
        with (tree):
            producers: Dict[SystemId, ProducerProfile] = {}
            # producers_tree = tree.fetch_tree("producers")
            # for k, v in producers_tree.fetch_tree_items():
            #     producer_id = SystemId(k)
            #     producer = ProducerProfileImpl.create(v)
            #     producers[producer_id] = producer

            consumers: Dict[SystemId, ConsumerProfile] = {}
            # consumers_tree = tree.fetch_tree("consumers")
            # for k, v in consumers_tree.fetch_tree_items():
            #     consumer_id = SystemId(k)
            #     consumer = ConsumerProfileImpl.create(v)
            #     consumers[consumer_id] = consumer

            return VhostProfileImpl(
                producers=producers,
                consumers=consumers
            )

    _producers: Dict[SystemId, ProducerProfile]
    _consumers: Dict[SystemId, ConsumerProfile]

    def __init__(
        self,
        producers: Dict[SystemId, ProducerProfile],
        consumers: Dict[SystemId, ConsumerProfile]
    ) -> None:
        self._producers = producers
        self._consumers = consumers

    def producers(self) -> Dict[SystemId, ProducerProfile]:
        return self._producers

    def consumers(self) -> Dict[SystemId, ConsumerProfile]:
        return self._consumers
