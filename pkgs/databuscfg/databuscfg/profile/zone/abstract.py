from typing import Dict
from typing import NewType

from databuscfg.profile.vhost.abstract import VhostId
from databuscfg.profile.vhost.abstract import VhostProfile

ZoneId = NewType("ZoneId", str)


class ZoneProfile:
    def vhosts(self) -> Dict[VhostId, VhostProfile]:
        ...
