from jinja2 import Template


class TemplateLoader:
    def load(self, name: str) -> Template:
        ...
