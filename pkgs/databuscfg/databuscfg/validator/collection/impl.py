from typing import Set

from databuscfg.validator.abstract import Validator
from pygentype.exception import ExpectedException


class CollectionValidatorImpl(Validator[str]):
    _collections: Set[str]

    def __init__(self, collections: Set[str]) -> None:
        self._collections = collections

    def validate(self, instance: str) -> None:
        if (instance not in self._collections):
            raise ExpectedException("Collection profile not found")
