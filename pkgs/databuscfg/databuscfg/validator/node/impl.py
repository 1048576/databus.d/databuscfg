from typing import Set

from databuscfg.validator.abstract import Validator
from pygentype.exception import ExpectedException


class NodeValidatorImpl(Validator[str]):
    _nodes: Set[str]

    def __init__(self, nodes: Set[str]) -> None:
        self._nodes = nodes

    def validate(self, instance: str) -> None:
        if (instance not in self._nodes):
            raise ExpectedException("Node profile not found")
