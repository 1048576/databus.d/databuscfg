from typing import Set

from databuscfg.validator.abstract import Validator
from pygentype.exception import ExpectedException


class RouteValidatorImpl(Validator[str]):
    _routes: Set[str]

    def __init__(self, routes: Set[str]) -> None:
        self._routes = routes

    def validate(self, instance: str) -> None:
        if (instance not in self._routes):
            raise ExpectedException("Route profile not found")
