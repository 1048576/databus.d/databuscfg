from typing import Set

from databuscfg.validator.abstract import Validator
from pygentype.exception import ExpectedException


class SystemValidatorImpl(Validator[str]):
    _systems: Set[str]

    def __init__(self, systems: Set[str]) -> None:
        self._systems = systems

    def validate(self, instance: str) -> None:
        if (instance not in self._systems):
            raise ExpectedException("System profile not found")
