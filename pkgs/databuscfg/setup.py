#!/usr/bin/env python

import os

from typing import List

from setuptools import find_packages
import setuptools


def read_requirements() -> List[str]:
    with (open("requirements.txt", "r") as f):
        return f.read().splitlines()


if (__name__ == "__main__"):
    setuptools.setup(
        name="databuscfg",
        version=os.environ.get("BUILD_ARG_VERSION", "0.0.dev"),
        author="Vladyslav Kazakov",
        author_email="kazakov1048576@gmail.com",
        url="https://gitlab.com/1048576/databus.d/databuscfg",
        install_requires=read_requirements(),
        package_data={
            "databuscfg.wiki": [
                "./templates/*"
            ]
        },
        package_dir={
            "": "lib"
        },
        packages=find_packages(
            include=["databuscfg", "databuscfg.*"]
        ),
        entry_points={},
        scripts=[],
        license="MIT"
    )
