package com.gitlab._1048576.databusd.exporter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.support.CronTrigger;

import com.gitlab._1048576.databusd.exporter.configuration.ExporterConfiguration;

@SpringBootApplication
@EnableScheduling
@ComponentScan({"com.gitlab._1048576.databusd.*"})
public class ExporterImpl {
    public static void main(final String[] args) {
        SpringApplication.run(ExporterImpl.class, args);
    }

    public ExporterImpl(
        @Autowired
        final TaskScheduler taskScheduler,
        @Autowired
        final ExporterConfiguration configuration
    ) {
        for (final var entity : configuration.entities()) {
            for (final var instance : entity.instances()) {
                taskScheduler.schedule(
                    new ExporterTaskImpl(entity.name(), instance.routingKey()),
                    new CronTrigger("* * * * * *")
                );
            }
        }
    }
}
