package com.gitlab._1048576.databusd.exporter.configuration;

import com.gitlab._1048576.databusd.exporter.configuration.entity.ExporterEntityConfiguration;

public interface ExporterConfiguration {
    public Iterable<ExporterEntityConfiguration> entities();
}
