package com.gitlab._1048576.databusd.exporter.configuration.entity;

import com.gitlab._1048576.databusd.exporter.configuration.instance.ExporterInstanceConfiguration;

public interface ExporterEntityConfiguration {
    public String name();

    public Iterable<ExporterInstanceConfiguration> instances();
}
