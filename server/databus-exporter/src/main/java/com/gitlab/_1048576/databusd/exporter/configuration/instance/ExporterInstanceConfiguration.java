package com.gitlab._1048576.databusd.exporter.configuration.instance;

public interface ExporterInstanceConfiguration {
    public String routingKey();
}
