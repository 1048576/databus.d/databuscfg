package com.gitlab._1048576.databusd.exporter.configuration.instance;

import java.io.IOException;

import com.gitlab._1048576.databusd.configuration.node.ConfigurationObjectNode;

public class ExporterInstanceConfigurationImpl implements ExporterInstanceConfiguration {
    private final String routingKey;

    public ExporterInstanceConfigurationImpl(
        final ConfigurationObjectNode node
    ) throws IOException {
        try (node) {
            this.routingKey = node.detachTextNode("routingKey");
        }
    }

    public String routingKey() {
        return this.routingKey;
    }
}
