package com.gitlab._1048576.databusd.gateway.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.gitlab._1048576.databusd.component.publisher.Publisher;
import com.gitlab._1048576.databusd.component.publisher.message.PublisherMessage;
import com.gitlab._1048576.databusd.component.publisher.message.PublisherMessageImpl;
import com.gitlab._1048576.databusd.component.publisher.message.logger.PublisherMessageStateLogger;
import com.gitlab._1048576.databusd.component.publisher.message.logger.PublisherMessageStateLoggerImpl;
import com.gitlab._1048576.databusd.gateway.db.configuration.GatewayDbConfiguration;
import com.gitlab._1048576.databusd.gateway.db.query.GatewayDbQuery;
import com.gitlab._1048576.databusd.message.MessageFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.gitlab._1048576.databusd.*"})
public class DatabusGatewayDbImpl implements ApplicationRunner {
    public static void main(final String[] args) {
        SpringApplication.run(DatabusGatewayDbImpl.class, args);
    }

    private final DataSource dataSourceMain;
    private final DataSource dataSourceWorker;
    private final Publisher publisher;
    private final GatewayDbQuery query;
    private final Logger appLogger;
    private final PublisherMessageStateLogger messageStateLogger;

    public DatabusGatewayDbImpl(
        @Autowired
        final DataSource dataSourceMain,
        @Autowired
        final DataSource dataSourceWorker,
        @Autowired
        final Publisher publisher,
        @Autowired
        final MessageFactory messageFactory,
        @Autowired
        final GatewayDbConfiguration configuration
    ) {
        this.dataSourceMain = dataSourceMain;
        this.dataSourceWorker = dataSourceWorker;
        this.publisher = publisher;
        this.query = configuration.query();
        this.appLogger = LoggerFactory.getLogger(this.getClass());
        this.messageStateLogger = new PublisherMessageStateLoggerImpl();
    }

    @Override
    public void run(final ApplicationArguments args) throws InterruptedException {
        while (true) {
            try {
                this.process();
            } catch (final SQLException e) {
               this.appLogger.error("Сrashing report", e);
            }

            Thread.sleep(2000);

            this.appLogger.info("Recovering...");
        }
    }

    private void process() throws SQLException, InterruptedException {
        final Connection connection = this.dataSourceMain.getConnection();
        try (connection) {
            final PreparedStatement statement = connection.prepareStatement(
                this.query.selectId()
            );
            try (statement) {
                this.appLogger.info("Started");
                while (true) {
                    this.appLogger.debug("Checking new messages");
                    final ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        this.process(
                            resultSet.getObject(1)
                        );
                    }

                    Thread.sleep(2000);
                }
            }
        }
    }

    private void process(final Object id) throws SQLException {
        final Connection connection = this.dataSourceWorker.getConnection();
        try (connection) {
            final PublisherMessage message = this.selectAndLockMessageOrReturnNull(
                connection, id
            );
            if (message != null) {
                final boolean processed = false;
                if (processed) {
                    this.updateMessageStateAndCommit(
                        connection,
                        id,
                        this.query.updateMessageStateProcessed()
                    );
                    this.messageStateLogger.processed(message);
                } else {
                    this.updateMessageStateAndCommit(
                        connection,
                        id,
                        this.query.updateMessageStateFailed()
                    );
                    this.appLogger.error("Failed");
                }
            } else {
                connection.rollback();
            }
        }
    }

    private PublisherMessage selectAndLockMessageOrReturnNull(
        final Connection connection,
        final Object id
    ) throws SQLException {
        final PreparedStatement statement = connection.prepareStatement(
            this.query.selectAndLockMessage()
        );
        try (statement) {
            statement.setObject(1, id);

            final ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                final String routingKey = rs.getString(1);

                return PublisherMessageImpl.create(routingKey);
            } else {
                return null;
            }
        } catch (final SQLException e) {
            throw this.query.decodeException(e);
        }
    }

    private void updateMessageStateAndCommit(
        final Connection connection,
        final Object id,
        final String query
    ) throws SQLException {
        final PreparedStatement statement = connection.prepareStatement(query);
        try (statement) {
            statement.setObject(1, id);

            statement.executeUpdate();
        }

        connection.commit();
    }
}
