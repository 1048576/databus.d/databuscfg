package com.gitlab._1048576.databusd.gateway.db.configuration;

import java.io.IOException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.gitlab._1048576.databusd.ExpectedException;
import com.gitlab._1048576.databusd.configuration.AppConfiguration;
import com.gitlab._1048576.databusd.configuration.node.ConfigurationObjectNode;
import com.gitlab._1048576.databusd.gateway.db.query.GatewayDbQuery;
import com.gitlab._1048576.databusd.gateway.db.query.GatewayDbQuerySQLServerImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class GatewayDbConfigurationImpl implements GatewayDbConfiguration {
    private final String url;
    private final String username;
    private final String password;
    private final GatewayDbQuery query;

    public GatewayDbConfigurationImpl(
        @Autowired
        final AppConfiguration appConfiguration
    ) throws IOException {
        final ConfigurationObjectNode databaseNode = appConfiguration.detachObjectNode(
            "database"
        );
        this.url = databaseNode.detachTextNode("url");
        this.username = databaseNode.detachTextNode("username");
        this.password = databaseNode.detachTextNode("password");
        final String table = databaseNode.detachTextNode("table");
        final String columnPrimaryKey = databaseNode.detachTextNode("columnPrimaryKey");
        final String columnPublishAt = databaseNode.detachTextNode("columnPublishAt");
        final String columnPublishAfter = databaseNode.detachTextNode("columnPublishAfter");
        final String columnPublishTTL = databaseNode.detachTextNode("columnPublishTTL");
        final String columnRoutingKey = databaseNode.detachTextNode("columnRoutingKey");
        final String columnBody = databaseNode.detachTextNode("columnBody");
        final String columnContentType = databaseNode.detachTextNode("columnContentType");
        final String columnHeaders = databaseNode.detachTextNode("columnHeaders");
        databaseNode.close();

        if (url.startsWith("jdbc:sqlserver://")) {
            this.query = new GatewayDbQuerySQLServerImpl(
                table,
                columnPrimaryKey,
                columnPublishAt,
                columnPublishAfter,
                columnPublishTTL,
                columnRoutingKey,
                columnBody,
                columnContentType,
                columnHeaders
            );
        } else {
            throw new ExpectedException("Unsupported database type");
        }
    }

    public GatewayDbQuery query() {
        return this.query;
    }

    @Bean
    public DataSource dataSourceMain() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setUrl(this.url);
        dataSource.setUsername(this.username);
        dataSource.setPassword(this.password);

        return dataSource;
    }

    @Bean
    public DataSource dataSourceWorker() {
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        hikariConfig.setAutoCommit(false);

        return new HikariDataSource(hikariConfig);
    }
}
