package com.gitlab._1048576.databusd.gateway.db.query;

import java.sql.SQLException;

public interface GatewayDbQuery {
    public String selectId();

    public String selectAndLockMessage();

    public String updateMessageStateProcessed();

    public String updateMessageStateFailed();

    public SQLException decodeException(final SQLException e);
}
