package com.gitlab._1048576.databusd.gateway.db.query;

import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map.Entry;

import com.gitlab._1048576.databusd.Text;

public class GatewayDbQuerySQLServerImpl implements GatewayDbQuery {
    private final String selectId;
    private final String selectAndLockMessage;
    private final String updateMessageStateProcessed;
    private final String updateMessageStateFailed;

    public GatewayDbQuerySQLServerImpl(
        final String table,
        final String columnPrimaryKey,
        final String columnPublishAt,
        final String columnPublishAfter,
        final String columnPublishTTL,
        final String columnRoutingKey,
        final String columnBody,
        final String columnContentType,
        final String columnHeaders
    ) {
        final Iterable<Entry<String, String>> templateArgs = Arrays.asList(
            new AbstractMap.SimpleEntry<>(
                "table",
                table
            ),
            new AbstractMap.SimpleEntry<>(
                "columnPrimaryKey",
                columnPrimaryKey
            ),
            new AbstractMap.SimpleEntry<>(
                "columnPublishAt",
                columnPublishAt
            ),
            new AbstractMap.SimpleEntry<>(
                "columnPublishAfter",
                columnPublishAfter
            ),
            new AbstractMap.SimpleEntry<>(
                "columnPublishTTL",
                columnPublishTTL
            ),
            new AbstractMap.SimpleEntry<>(
                "columnRoutingKey",
                columnRoutingKey
            ),
            new AbstractMap.SimpleEntry<>(
                "columnBody",
                columnBody
            ),
            new AbstractMap.SimpleEntry<>(
                "columnContentType",
                columnContentType
            ),
            new AbstractMap.SimpleEntry<>(
                "columnHeaders",
                columnHeaders
            )
        );

        this.selectId = Text.format(
            String.join(
                "\n",
                "select",
                "    ${columnPrimaryKey}",
                "from ${table}",
                "where (",
                "    (${columnPublishAt} is null) and",
                "    (${columnPublishAfter} < current_timestamp) and",
                "    (${columnPublishTTL} > current_timestamp)",
                ")",
                "order by ${columnPublishAfter}"
            ),
            templateArgs
        );

        this.selectAndLockMessage = Text.format(
            String.join(
                "\n",
                "set lock_timeout 1000;",
                "select",
                "    ${columnRoutingKey},",
                "    ${columnBody},",
                "    ${columnContentType},",
                "    ${columnheaders}",
                "from ${table}",
                "with (updlock)",
                "where (",
                "    (${columnPrimaryKey} = ?) and",
                "    (${columnPublishAt} is null)",
                ")"
            ),
            templateArgs
        );

        this.updateMessageStateProcessed = Text.format(
            String.join(
                "\n",
                "update ${table}",
                "set ${columnPublishAt} = current_timestamp",
                "where (${columnPrimaryKey} = ?)"
            ),
            templateArgs
        );

        this.updateMessageStateFailed = Text.format(
            String.join(
                "\n",
                "update ${table}",
                "set ${columnPublishAfter} = dateadd(minute, 1, current_timestamp)",
                "where (${columnPrimaryKey} = ?)"
            ),
            templateArgs
        );
    }

    @Override
    public String selectId() {
        return this.selectId;
    }

    @Override
    public String selectAndLockMessage() {
        return this.selectAndLockMessage;
    }

    @Override
    public String updateMessageStateProcessed() {
        return this.updateMessageStateProcessed;
    }

    @Override
    public String updateMessageStateFailed() {
        return this.updateMessageStateFailed;
    }

    @Override
    public SQLException decodeException(final SQLException e) {
        return e;
    }
}
