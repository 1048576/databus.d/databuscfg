package com.gitlab._1048576.databusd.gateway.http;

import com.gitlab._1048576.databusd.gateway.http.processor.rabbitmq.DatabusHttpGatewayRabbitmqProcessorImpl;
import com.gitlab._1048576.databusd.gateway.http.processor.rabbitmq.configurationreader.DatabusHttpGatewayRabbitmqConfigurationReaderImpl;
import com.gitlab._1048576.databusd.gateway.http.server.configurationreader.DatabusHttpGatewayServerConfigurationReaderImpl;
import com.gitlab._1048576.databusd.logging.logger.DatabusLogger;
import com.gitlab._1048576.databusd.logging.logger.DatabusLoggerImpl;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.jakson.JvgenCfgJaksonAppYamlConfigurationReaderImpl;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import com.gitlab._1048576.libd.jvgen.httpserver.security.JvgenHttpServerSecurityAuthenticator;
import com.gitlab._1048576.libd.jvgen.logging.NotLoggingException;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBroker;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class DatabusHttpGatewayApplicationBeanFactoryImpl {
    private final DatabusHttpGatewayServerConfiguration serverConfiguration;
    private final DatabusHttpGatewayRabbitmqConfiguration rabbitmqConfiguration;
    private final Logger logger;

    public DatabusHttpGatewayApplicationBeanFactoryImpl(
        final ApplicationArguments args,
        final BuildProperties buildProperties,
        final GenericApplicationContext context
    ) throws Exception {
        final var logger = LoggerFactory.getLogger(
            "configuration"
        );

        final var appConfigurationReader = new JvgenCfgJaksonAppYamlConfigurationReaderImpl();
        final var serverConfigurationReader = new DatabusHttpGatewayServerConfigurationReaderImpl();
        final var rabbitmqConfigurationReader = new DatabusHttpGatewayRabbitmqConfigurationReaderImpl();

        try {
            final var appNode = appConfigurationReader.read(
                args,
                buildProperties
            );

            this.serverConfiguration = serverConfigurationReader.read(appNode);
            this.rabbitmqConfiguration = rabbitmqConfigurationReader.read(appNode);
            this.logger = LoggerFactory.getLogger("init-application");

            appNode.close();
        } catch (final JvgenUserException e) {
            logger.error(e.getMessage());

            throw new NotLoggingException(e);
        }
    }

    @Bean
    public DatabusLogger databusLogger() {
        return new DatabusLoggerImpl();
    }

    @Bean
    public JvgenHttpServerSecurityAuthenticator authenticator() {
        return this.serverConfiguration.createAuthenticator();
    }

    @Bean
    public JvgenRabbitmqBroker broker() throws Exception {
        return this.rabbitmqConfiguration.createBroker();
    }

    @Bean
    public JvgenRabbitmqPublisher publisher(
        final JvgenRabbitmqBroker broker
    ) throws Exception {
        try {
            return broker.createPublisher(
                this.rabbitmqConfiguration.exchange()
            );
        } catch (final JvgenUserException e) {
            this.logger.error(e.getMessage());

            throw new NotLoggingException(e);
        }
    }

    @Bean
    public DatabusHttpGatewayProcessor processor(
        final JvgenRabbitmqPublisher publisher
    ) throws Exception {
        return new DatabusHttpGatewayRabbitmqProcessorImpl(publisher);
    }
}
