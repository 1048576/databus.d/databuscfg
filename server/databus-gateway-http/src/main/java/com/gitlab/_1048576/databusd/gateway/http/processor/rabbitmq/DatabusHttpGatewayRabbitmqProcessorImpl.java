package com.gitlab._1048576.databusd.gateway.http.processor.rabbitmq;

import com.gitlab._1048576.databusd.gateway.http.DatabusHttpGatewayProcessor;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqPublisher;
import java.util.Map;

public class DatabusHttpGatewayRabbitmqProcessorImpl implements DatabusHttpGatewayProcessor {
    private final JvgenRabbitmqPublisher publisher;

    public DatabusHttpGatewayRabbitmqProcessorImpl(
        final JvgenRabbitmqPublisher publisher
    ) {
        this.publisher = publisher;
    }

    @Override
    public String process(
        final byte[] body,
        final String contentType,
        final String routingKey,
        final Map<String, ? extends Object> headers
    ) throws Exception {
        return this.publisher.publish(
            body,
            contentType,
            routingKey,
            headers
        );
    }
}
