package com.gitlab._1048576.databusd.gateway.http.server.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gitlab._1048576.databusd.gateway.http.server.DatabusHttpGatewayServerRoutes;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DatabusHttpGatewayServerUserControllerImpl {
    private static final String KEY_USERNAME = "username";
    private static final String KEY_AUTHORITIES = "authorities";

    private final ObjectMapper objectMapper;

    public DatabusHttpGatewayServerUserControllerImpl() {
        this.objectMapper = new ObjectMapper();
    }

    @GetMapping(
        path = DatabusHttpGatewayServerRoutes.GET_USER,
        produces = "application/json"
    )
    public ResponseEntity<String> processGetUserRequest(
        final Authentication authenitcation
    ) throws JsonProcessingException {
        final var user = this.user(authenitcation);

        return ResponseEntity.ok().body(
            this.objectMapper.writeValueAsString(user)
        );
    }

    private ObjectNode user(final Authentication authenitcation) {
        if (authenitcation == null) {
            return this.unauthorizedUser();
        } else {
            return this.authorizedUser(authenitcation);
        }
    }

    private ObjectNode unauthorizedUser() {
        final var body = this.objectMapper.createObjectNode();

        body.set(KEY_USERNAME, this.objectMapper.nullNode());
        body.set(KEY_AUTHORITIES, this.objectMapper.createArrayNode());

        return body;
    }

    private ObjectNode authorizedUser(final Authentication authenitcation) {
        final var body = this.objectMapper.createObjectNode();

        body.put(KEY_USERNAME, authenitcation.getName());
        body.set(KEY_AUTHORITIES, this.createAuthoritiesNode(authenitcation));

        return body;
    }

    private ArrayNode createAuthoritiesNode(final Authentication authenitcation) {
        final var result = this.objectMapper.createArrayNode();

        for (final var authority : authenitcation.getAuthorities()) {
            result.add(authority.getAuthority());
        }

        return result;
    }
}
