package com.gitlab._1048576.databusd.gateway.http.server.security;

import com.gitlab._1048576.databusd.gateway.http.server.DatabusHttpGatewayServerRoutes;
import com.gitlab._1048576.libd.jvgen.httpserver.security.JvgenHttpServerSecurityAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Service;

@Service
public final class DatabusHttpGatewayServerSecurityImpl {
    private final Logger logger;

    public DatabusHttpGatewayServerSecurityImpl() {
        this.logger = LoggerFactory.getLogger("security-log");
    }

    @Bean
    public AuthenticationProvider authenticationProvider(
        final JvgenHttpServerSecurityAuthenticator authenticator
    ) throws Exception {
        return authenticator.createAuthenticationProvider();
    }

    @Bean
    public SecurityFilterChain filterChain(
        final JvgenHttpServerSecurityAuthenticator authenticator,
        final AuthenticationConfiguration authenticationConfiguration,
        final HttpSecurity http
    ) throws Exception {
        http.csrf().disable();

        http.addFilterBefore(
            authenticator.createFilter(
                authenticationConfiguration.getAuthenticationManager()
            ),
            BasicAuthenticationFilter.class
        );

        http.exceptionHandling().authenticationEntryPoint(
            (request, response, authException) -> {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());

                for (final var entry : authenticator.headers().entrySet()) {
                    response.addHeader(entry.getKey(), entry.getValue());
                }
            }
        );

        http.exceptionHandling().accessDeniedHandler(
            (request, response, accessDeniedException) -> {
                final var context = SecurityContextHolder.getContext();
                final var authentication = context.getAuthentication();

                this.logger.warn(
                    String.format(
                        "Access denied [%s]",
                        authentication.getName()
                    )
                );

                response.setStatus(HttpStatus.FORBIDDEN.value());
            }
        );

        this.addGetUserUrl(http);

        this.addAnyUrl(http);

        return http.build();
    }

    private void addGetUserUrl(final HttpSecurity http) throws Exception {
        final var url = http.authorizeHttpRequests().requestMatchers(
            DatabusHttpGatewayServerRoutes.GET_USER
        );

        url.permitAll();
    }

    private void addAnyUrl(final HttpSecurity http) throws Exception {
        final var url = http.authorizeHttpRequests().anyRequest();

        url.hasAuthority(DatabusHttpGatewayServerSecurityUserRole.FULL.name());
    }
}
