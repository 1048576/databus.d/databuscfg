package com.gitlab._1048576.databusd.gateway.http.server.security.userdetails;

import com.gitlab._1048576.databusd.gateway.http.server.security.DatabusHttpGatewayServerSecurityUserRole;
import java.util.Arrays;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public final class DatabusHttpGatewayServerSecurityFakeUserDetailsServiceImpl implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(
        final String username
    ) throws UsernameNotFoundException {
        return new User(
            username,
            "",
            Arrays.asList(
                new SimpleGrantedAuthority(DatabusHttpGatewayServerSecurityUserRole.FULL.name())
            )
        );
    }
}
