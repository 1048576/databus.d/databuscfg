package com.gitlab._1048576.databusd.worker.handler.rabbitmq.queuehandler;

import com.gitlab._1048576.databusd.logging.logger.DatabusLogger;
import com.gitlab._1048576.databusd.worker.DatabusWorkerHandler;
import com.gitlab._1048576.databusd.worker.DatabusWorkerHandlerInstance;
import com.gitlab._1048576.databusd.worker.DatabusWorkerProcessor;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import com.gitlab._1048576.libd.jvgen.logging.NotLoggingException;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBroker;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqMessageListener;
import java.util.Map;

public class DatabusWorkerRabbitmqQueueHandlerImpl implements DatabusWorkerHandler, JvgenRabbitmqMessageListener {
    private final DatabusLogger databusLogger;
    private final JvgenRabbitmqBroker broker;
    private final String queue;
    private final DatabusWorkerProcessor processor;

    public DatabusWorkerRabbitmqQueueHandlerImpl(
        final DatabusLogger databusLogger,
        final JvgenRabbitmqBroker broker,
        final String queue,
        final DatabusWorkerProcessor processor
    ) {
        this.databusLogger = databusLogger;
        this.broker = broker;
        this.queue = queue;
        this.processor = processor;
    }

    @Override
    public DatabusWorkerHandlerInstance start() {
        final var queue = this.queue;
        final var consumer = this.broker.createConsumer(this.queue, this);

        return new DatabusWorkerHandlerInstance() {
            @Override
            public void stop() throws Exception {
                consumer.stop();
            }

            @Override
            public String toString() {
                return queue;
            }
        };
    }

    @Override
    public void onMessage(
        final String trackingId,
        final byte[] body,
        final String contentType,
        final String routingKey,
        final Map<String, String> headers
    ) throws Exception {
        try {
            this.processor.process(
                body,
                contentType,
                headers
            );
        } catch (final JvgenUserException e) {
            this.databusLogger.messageProcessingFailed(
                routingKey,
                headers,
                e.getMessage()
            );

            throw new NotLoggingException(e);
        }

        this.databusLogger.messageProcessingCompleted(
            trackingId,
            routingKey,
            headers,
            body.length
        );
    }
}
