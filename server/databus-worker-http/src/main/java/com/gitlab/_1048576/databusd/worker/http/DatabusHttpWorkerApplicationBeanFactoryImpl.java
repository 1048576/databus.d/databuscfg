package com.gitlab._1048576.databusd.worker.http;

import com.gitlab._1048576.databusd.logging.logger.DatabusLogger;
import com.gitlab._1048576.databusd.logging.logger.DatabusLoggerImpl;
import com.gitlab._1048576.databusd.worker.DatabusWorkerHandler;
import com.gitlab._1048576.databusd.worker.handler.rabbitmq.configurationreader.DatabusWorkerRabbitmqHandlerConfigurationReaderImpl;
import com.gitlab._1048576.databusd.worker.handler.rabbitmq.queuehandler.DatabusWorkerRabbitmqQueueHandlerImpl;
import com.gitlab._1048576.databusd.worker.http.configurationreader.DatabusHttpWorkerEndpointsConfigurationReaderImpl;
import com.gitlab._1048576.databusd.worker.http.configurationreader.DatabusHttpWorkerProcessorKeytabsConfigurationReaderImpl;
import com.gitlab._1048576.databusd.worker.http.handler.rabbitmq.configurationreader.DatabusHttpWorkerRabbitmqHandlersConfigurationReaderImpl;
import com.gitlab._1048576.databusd.worker.http.processor.DatabusHttpWorkerProcessorImpl;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.jakson.JvgenCfgJaksonAppYamlConfigurationReaderImpl;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import com.gitlab._1048576.libd.jvgen.logging.NotLoggingException;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBroker;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBrokerConfiguration;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class DatabusHttpWorkerApplicationBeanFactoryImpl {
    private final JvgenRabbitmqBrokerConfiguration brokerConfiguration;
    private final DatabusHttpWorkerRabbitmqHandlersConfiguration handlersConfiguration;

    public DatabusHttpWorkerApplicationBeanFactoryImpl(
        final ApplicationArguments args,
        final BuildProperties buildProperties,
        final GenericApplicationContext context
    ) throws Exception {
        final var logger = LoggerFactory.getLogger(
            "configuration"
        );

        final var appConfigurationReader = new JvgenCfgJaksonAppYamlConfigurationReaderImpl();
        final var rabbitmqHandlerConfigurationReader = new DatabusWorkerRabbitmqHandlerConfigurationReaderImpl();
        final var keytabsConfigurationReader = new DatabusHttpWorkerProcessorKeytabsConfigurationReaderImpl();
        final var endpointsConfigurationReader = new DatabusHttpWorkerEndpointsConfigurationReaderImpl();
        final var handlersConfigurationReader = new DatabusHttpWorkerRabbitmqHandlersConfigurationReaderImpl();

        try {
            final var appNode = appConfigurationReader.read(
                args,
                buildProperties
            );

            final var rabbitmqHandlerConfiguration = rabbitmqHandlerConfigurationReader.read(
                appNode
            );
            final var endpointsConfiguration = endpointsConfigurationReader.read(
                appNode
            );
            final var keytabsConfiguration = keytabsConfigurationReader.read(
                appNode
            );
            final var handlersConfiguration = handlersConfigurationReader.read(
                rabbitmqHandlerConfiguration.queues(),
                endpointsConfiguration.endpoints(),
                keytabsConfiguration.keytabs()
            );

            this.brokerConfiguration = rabbitmqHandlerConfiguration.broker();
            this.handlersConfiguration = handlersConfiguration;

            appNode.close();
        } catch (final JvgenUserException e) {
            logger.error(e.getMessage());

            throw new NotLoggingException(e);
        }
    }

    @Bean
    public DatabusLogger databusLogger() {
        return new DatabusLoggerImpl();
    }

    @Bean
    public JvgenRabbitmqBroker broker() throws Exception {
        return this.brokerConfiguration.create();
    }

    @Bean
    public Iterable<DatabusWorkerHandler> handlers(
        final DatabusLogger databusLogger,
        final JvgenRabbitmqBroker broker
    ) throws Exception {
        final var handlers = new ArrayList<DatabusWorkerHandler>();

        for (final var handlerConfiguration : this.handlersConfiguration.handlers()) {
            final var processor = new DatabusHttpWorkerProcessorImpl(
                handlerConfiguration.endpoint().url(),
                handlerConfiguration.keytab().principal(),
                handlerConfiguration.keytab().filepath()
            );
            final var handler = new DatabusWorkerRabbitmqQueueHandlerImpl(
                databusLogger,
                broker,
                handlerConfiguration.queue(),
                processor
            );

            handlers.add(handler);
        }

        return handlers;
    }
}
