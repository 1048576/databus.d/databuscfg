package com.gitlab._1048576.databusd.worker.http;

import com.gitlab._1048576.databusd.worker.DatabusWorkerApplication;
import com.gitlab._1048576.databusd.worker.DatabusWorkerApplicationImpl;
import com.gitlab._1048576.databusd.worker.DatabusWorkerHandler;
import jakarta.annotation.PreDestroy;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabusHttpWorkerApplicationImpl implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(DatabusHttpWorkerApplicationImpl.class, args);
    }

    private final DatabusWorkerApplication application;

    public DatabusHttpWorkerApplicationImpl(
        final Iterable<DatabusWorkerHandler> workers
    ) {
        this.application = new DatabusWorkerApplicationImpl(
            LoggerFactory.getLogger(DatabusHttpWorkerApplicationImpl.class),
            workers
        );
    }

    @Override
    public void run(final ApplicationArguments args) throws Exception {
        this.application.run();
    }

    @PreDestroy
    public void shutdown() {
        this.application.shutdown();
    }
}
