package com.gitlab._1048576.databusd.worker.http;

import java.util.Map;

public interface DatabusHttpWorkerEndpointsConfiguration {
    public Map<String, DatabusHttpWorkerEndpointConfiguration> endpoints();
}
