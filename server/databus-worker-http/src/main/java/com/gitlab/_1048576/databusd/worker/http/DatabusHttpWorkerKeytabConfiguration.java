package com.gitlab._1048576.databusd.worker.http;

public interface DatabusHttpWorkerKeytabConfiguration {
    public String name();

    public String principal();

    public String filepath();
}
