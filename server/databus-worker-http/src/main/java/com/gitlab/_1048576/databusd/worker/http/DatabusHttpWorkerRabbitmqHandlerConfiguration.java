package com.gitlab._1048576.databusd.worker.http;

public interface DatabusHttpWorkerRabbitmqHandlerConfiguration {
    public String queue();

    public DatabusHttpWorkerEndpointConfiguration endpoint();

    public DatabusHttpWorkerKeytabConfiguration keytab();
}
