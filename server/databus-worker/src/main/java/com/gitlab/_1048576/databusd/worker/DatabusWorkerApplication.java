package com.gitlab._1048576.databusd.worker;

public interface DatabusWorkerApplication {
    public void run() throws Exception;

    public void shutdown();
}
