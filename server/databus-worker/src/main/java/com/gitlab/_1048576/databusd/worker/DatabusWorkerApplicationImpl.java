package com.gitlab._1048576.databusd.worker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;

public class DatabusWorkerApplicationImpl implements DatabusWorkerApplication {
    private final Logger logger;
    private final Iterable<DatabusWorkerHandler> handlers;
    private Iterable<DatabusWorkerHandlerInstance> handlerInstances;

    public DatabusWorkerApplicationImpl(
        final Logger logger,
        final Iterable<DatabusWorkerHandler> handlers
    ) {
        this.logger = logger;
        this.handlers = handlers;
        this.handlerInstances = Collections.emptyList();
    }

    @Override
    public void run() throws Exception {
        this.logger.info("Application - starting...");

        this.handlerInstances = this.createHandlerInstances();

        this.logger.info("Application - start completed");
    }

    @Override
    public void shutdown() {
        this.logger.info("Application - shutdowning...");

        for (final var handlerInstance : this.handlerInstances) {
            try {
                handlerInstance.stop();

                this.logger.info(
                    "Handler [{}] stopped",
                    handlerInstance
                );
            } catch (final Exception ignored) {
                this.logger.warn(
                    "Cannot shutdown handler [{}] properly",
                    handlerInstance,
                    ignored
                );
            }
        }

        this.logger.info("Application - shutdown completed");
    }

    private List<DatabusWorkerHandlerInstance> createHandlerInstances() throws Exception {
        final var result = new ArrayList<DatabusWorkerHandlerInstance>();

        try {
            for (final var handler : this.handlers) {
                final var handlerInstance = handler.start();

                result.add(handlerInstance);

                this.logger.info(
                    "Handler [{}] started",
                    handlerInstance
                );
            }
        } catch (final Exception critical) {
            for (final var handlerInstance : result) {
                try {
                    handlerInstance.stop();
                } catch (final Exception ignored) {
                    // Do nothing
                }
            }

            throw critical;
        }

        return result;
    }
}
