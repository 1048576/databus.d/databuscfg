package com.gitlab._1048576.databusd.worker;

public interface DatabusWorkerHandler {
    public DatabusWorkerHandlerInstance start();
}
