package com.gitlab._1048576.libd.jvgen.cfg.configuration;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.info.BuildProperties;

public interface JvgenCfgAppConfigurationReader {
    public JvgenCfgConfigurationObjectNode read(
        final ApplicationArguments args,
        final BuildProperties buildProperties
    ) throws Exception;
}
