package com.gitlab._1048576.libd.jvgen.cfg.configuration.jakson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgAppConfigurationReader;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import java.nio.file.Paths;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.info.BuildProperties;

public final class JvgenCfgJaksonAppYamlConfigurationReaderImpl implements JvgenCfgAppConfigurationReader {
    @Override
    public JvgenCfgConfigurationObjectNode read(
        final ApplicationArguments args,
        final BuildProperties buildProperties
    ) throws Exception {
        if (args.getSourceArgs().length != 1) {
            throw new JvgenUserException(
                "Invalid application arguments count"
            );
        }

        final var jacksonMapper = new ObjectMapper(new YAMLFactory());

        final var jacksonNode = jacksonMapper.readTree(
            Paths.get(args.getSourceArgs()[0]).toFile()
        );

        final var rootNode = new JvgenCfgJaksonConfigurationObjectNodeImpl(
            "$",
            jacksonNode
        );

        final var appNode = rootNode.detachObjectNode(
            buildProperties.getArtifact()
        );

        rootNode.close();

        return appNode;
    }
}
