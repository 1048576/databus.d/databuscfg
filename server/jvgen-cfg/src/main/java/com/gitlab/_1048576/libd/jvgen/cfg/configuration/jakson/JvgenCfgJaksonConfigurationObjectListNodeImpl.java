package com.gitlab._1048576.libd.jvgen.cfg.configuration.jakson;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectListNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;

public final class JvgenCfgJaksonConfigurationObjectListNodeImpl implements JvgenCfgConfigurationObjectListNode {
    private final String path;
    private final ArrayNode jacksonNode;
    private int index;

    public JvgenCfgJaksonConfigurationObjectListNodeImpl(
        final String path,
        final ArrayNode jacksonNode
    ) {
        this.path = path;
        this.jacksonNode = jacksonNode;
        this.index = -1;
    }

    @Override
    public JvgenCfgConfigurationObjectNode detachObjectNode() throws Exception {
        final var jacksonNode = this.jacksonNode.remove(0);

        return new JvgenCfgJaksonConfigurationObjectNodeImpl(
            String.format("%s.[%d]", this.path, ++this.index),
            jacksonNode
        );
    }

    @Override
    public boolean isEmpty() {
        return this.jacksonNode.isEmpty();
    }

    @Override
    public void close() throws Exception {
        if (!this.jacksonNode.isEmpty()) {
            throw new JvgenUserException(
                String.format(
                    "Unexpected nodes {%s.[%d]-%s.[%d]}",
                    this.path,
                    this.index + 1,
                    this.path,
                    this.jacksonNode.size() - 1
                )
            );
        }
    }
}
