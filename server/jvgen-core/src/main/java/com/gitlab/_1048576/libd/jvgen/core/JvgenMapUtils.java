package com.gitlab._1048576.libd.jvgen.core;

import java.util.HashMap;
import java.util.Map;

public final class JvgenMapUtils {
    public <K, V> Map<K, V> newHashMap(final K k0, final V v0) {
        final var result = new HashMap<K, V>();

        result.put(k0, v0);

        return result;
    }

    private JvgenMapUtils() {
    }
}
