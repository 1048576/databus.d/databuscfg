package com.gitlab._1048576.libd.jvgen.core;

import java.util.Map.Entry;

public final class JvgenTextUtils {
    public static String format(
        final String template,
        final Iterable<Entry<String, String>> args
    ) {
        String formattedText = template;

        for (final var arg : args) {
            formattedText = formattedText.replaceAll(
                String.format("\\{{ %s\\ }}", arg.getKey()),
                arg.getValue()
            );
        }

        return formattedText;
    }

    private JvgenTextUtils() {
    }
}
