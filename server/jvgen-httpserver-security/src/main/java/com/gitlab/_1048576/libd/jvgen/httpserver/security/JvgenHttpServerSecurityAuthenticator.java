package com.gitlab._1048576.libd.jvgen.httpserver.security;

import jakarta.servlet.Filter;
import java.util.Map;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;

public interface JvgenHttpServerSecurityAuthenticator {
    public Map<String, String> headers();

    public AuthenticationProvider createAuthenticationProvider(
    ) throws Exception;

    public Filter createFilter(
        final AuthenticationManager authenticationManager
    );
}
