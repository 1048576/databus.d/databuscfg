package com.gitlab._1048576.libd.jvgen.httpserver.security.authenticator;

import com.gitlab._1048576.libd.jvgen.httpserver.security.JvgenHttpServerSecurityAuthenticator;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public final class JvgenHttpServerSecurityFakeAuthenticatorImpl implements JvgenHttpServerSecurityAuthenticator, AuthenticationProvider, Filter, Authentication {
    private final Collection<? extends GrantedAuthority> authorities;

    public JvgenHttpServerSecurityFakeAuthenticatorImpl(
        Collection<? extends GrantedAuthority> authorities
    ) {
        this.authorities = authorities;
    }

    @Override
    public Map<String, String> headers() {
        return Collections.emptyMap();
    }

    @Override
    public AuthenticationProvider createAuthenticationProvider() {
        return this;
    }

    @Override
    public Filter createFilter(
        final AuthenticationManager authenticationManager
    ) {
        return this;
    }

    @Override
    public Authentication authenticate(
        final Authentication authentication
    ) throws AuthenticationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void doFilter(
        final ServletRequest request,
        final ServletResponse response,
        final FilterChain chain
    ) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(this);

        chain.doFilter(request, response);
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public Object getCredentials() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getDetails() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getPrincipal() {
        return this;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(final boolean isAuthenticated) {
        throw new UnsupportedOperationException();
    }
}
