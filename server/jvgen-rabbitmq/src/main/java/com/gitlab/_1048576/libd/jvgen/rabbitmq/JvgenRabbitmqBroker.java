package com.gitlab._1048576.libd.jvgen.rabbitmq;

public interface JvgenRabbitmqBroker {
    public JvgenRabbitmqPublisher createPublisher(
        final String exchange
    ) throws Exception;

    public JvgenRabbitmqConsumer createConsumer(
        final String queue,
        final JvgenRabbitmqMessageListener messageListener
    );
}
