package com.gitlab._1048576.libd.jvgen.rabbitmq;

public interface JvgenRabbitmqBrokerConfiguration {
    public JvgenRabbitmqBroker create() throws Exception;
}
