package com.gitlab._1048576.libd.jvgen.rabbitmq;

public interface JvgenRabbitmqConsumer {
    public void stop();
}
