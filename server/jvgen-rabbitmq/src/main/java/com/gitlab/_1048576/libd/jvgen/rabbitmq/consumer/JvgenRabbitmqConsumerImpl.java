package com.gitlab._1048576.libd.jvgen.rabbitmq.consumer;

import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqConsumer;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqMessageListener;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.AbstractMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;

public class JvgenRabbitmqConsumerImpl implements JvgenRabbitmqConsumer, MessageListener {
    private final Function<byte[], String> createTrackingIdFn;
    private final JvgenRabbitmqMessageListener messageListener;
    private final AbstractMessageListenerContainer container;

    public JvgenRabbitmqConsumerImpl(
        final Function<byte[], String> createTrackingIdFn,
        final ConnectionFactory connectionFactory,
        final String queue,
        final JvgenRabbitmqMessageListener messageListener
    ) {
        final var container = new SimpleMessageListenerContainer();

        container.setConnectionFactory(connectionFactory);
        container.addQueueNames(queue);
        container.setMessageListener(this);
        container.setPrefetchCount(1);
        container.setDefaultRequeueRejected(true);
        container.start();

        this.createTrackingIdFn = createTrackingIdFn;
        this.messageListener = messageListener;
        this.container = container;
    }

    @Override
    public void stop() {
        this.container.stop();
    }

    @Override
    public void onMessage(final Message message) {
        final var queue = message.getMessageProperties().getConsumerQueue();
        final var routingKey = this.getRoutingKey(message);
        final var messageHeaders = message.getMessageProperties().getHeaders();

        final var trackingId = this.createTrackingIdFn.apply(
            message.getBody()
        );

        final var headers = this.createHeaders(
            queue,
            routingKey,
            messageHeaders
        );

        try {
            messageListener.onMessage(
                trackingId,
                message.getBody(),
                message.getMessageProperties().getContentType(),
                routingKey,
                headers
            );
        } catch (final Exception e) {
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }

    private String getRoutingKey(final Message message) {
        final var headers = message.getMessageProperties().getHeaders();

        do {
            final var deathListUnchecked = headers.get("x-death");

            if (!(deathListUnchecked instanceof List<?>)) {
                break;
            }

            final var deathList = (List<?>) deathListUnchecked;

            if (deathList.isEmpty()) {
                break;
            }

            final var lastDeathUnchecked = deathList.get(deathList.size() - 1);

            if (!(lastDeathUnchecked instanceof Map<?, ?>)) {
                break;
            }

            final var lastDeath = (Map<?, ?>) lastDeathUnchecked;

            final var routingKeysUnchecked = lastDeath.get("routing-keys");

            if (!(routingKeysUnchecked instanceof List<?>)) {
                break;
            }

            final var routingKeys = (List<?>) routingKeysUnchecked;

            if (routingKeys.size() != 1) {
                break;
            }

            final var routingKeyUnchecked = routingKeys.get(0);

            if (routingKeyUnchecked instanceof String) {
                return (String) routingKeyUnchecked;
            }
        } while (false);

        return message.getMessageProperties().getReceivedRoutingKey();
    }

    private Map<String, String> createHeaders(
        final String queue,
        final String routingKey,
        final Map<String, Object> messageHeaders
    ) {
        final var stream = messageHeaders.entrySet().stream();

        final var filteredStream = stream.filter(
            (entry) -> {
                if (entry.getKey().startsWith("spring_")) {
                    return false;
                }

                if (entry.getKey().startsWith("x-death")) {
                    return false;
                }

                if (entry.getKey().startsWith("x-first-death")) {
                    return false;
                }

                return true;
            }
        );

        final var headers = filteredStream.collect(
            Collectors.toMap(
                (entry) -> {
                    return entry.getKey();
                },
                (entry) -> {
                    return entry.getValue().toString();
                }
            )
        );

        headers.put("queue", queue);
        headers.put("routingKey", routingKey);

        return headers;
    }
}
