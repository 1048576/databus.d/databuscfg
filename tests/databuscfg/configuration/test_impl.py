from unittest import TestCase
from unittest import skip

from databuscfg.configuration.impl import ConfigurationImpl
from databuscfgtest.loader.mock import LoaderMock
from pygentestcase.impl import TestCaseImpl
from pygentree.impl import TreeImpl
from pygentype.exception import ExpectedException


@skip("")
class ConfigurationLoadTest(TestCase):
    def test_load_systems_failed(self) -> None:
        loader = LoaderMock(
            items={
                "systems": None
            }
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.load(loader)

        err_msg = (
            "Cannot load systems [{}]:\n"
            "  Nodes has type [NoneType] instead of dict"
        )

        self.assertEqual(
            first=err_msg,
            second=str(err_ctx.exception)
        )

    def test_load_nodes_failed(self) -> None:
        loader = LoaderMock(
            items={
                "systems": {},
                "nodes": None
            }
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.load(loader)

        err_msg = (
            "Cannot load nodes [{}]:\n"
            "  Nodes has type [NoneType] instead of dict"
        )

        self.assertEqual(
            first=err_msg,
            second=str(err_ctx.exception)
        )

    def test_load_routes_failed(self) -> None:
        loader = LoaderMock(
            items={
                "systems": {},
                "nodes": {}
            }
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.load(loader)

        self.assertEqual(
            first="Cannot load routes",
            second=str(err_ctx.exception)
        )

    def test_load_environments_failed(self) -> None:
        loader = LoaderMock(
            items={
                "systems": {},
                "nodes": {}
            },
            collections={
                "routes": {},
                "collections": {}
            }
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.load(loader)

        self.assertEqual(
            first="Cannot load environments",
            second=str(err_ctx.exception)
        )

    def test_load_entities_failed(self) -> None:
        loader = LoaderMock(
            items={
                "systems": {},
                "nodes": {}
            },
            collections={
                "routes": {},
                "collections": {},
                "environments": {}
            }
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.load(loader)

        self.assertEqual(
            first="Cannot load entities",
            second=str(err_ctx.exception)
        )


@skip("")
class ConfigurationCreateRouteTest(TestCaseImpl):
    def test_create_route_already_defined(self) -> None:
        system_id = self.randomText()
        route_id = self.randomText()

        system_content: object = {
            "title": ""
        }
        systems_tree_items = [
            (system_id, TreeImpl.create(system_content))
        ]

        route_content: object = {
            "title": self.randomText(),
            "producer": system_id,
            "consumer": system_id,
            "exclusive": False,
            "rules": []
        }

        routes_tree_items = [
            (route_id, TreeImpl.create(route_content)),
            (route_id, TreeImpl.create(route_content))
        ]

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.create(
                system_tree_it=systems_tree_items,
                node_tree_it=[],
                route_tree_it=routes_tree_items,
                collection_tree_it=[],
                environment_tree_it=[],
                entity_tree_it=[]
            )

        err_msg_template = (
            "Routes:\n"
            "  Route [{}] already defined"
        )

        self.assertEqual(
            first=err_msg_template.format(
                route_id
            ),
            second=str(err_ctx.exception)
        )


@skip("")
class ConfigurationCreateCollectionTest(TestCaseImpl):
    def test_create_collection_already_defined(self) -> None:
        collection_id = self.randomText()

        collection_content: object = {
            "routes": []
        }

        collections_tree_items = [
            (collection_id, TreeImpl.create(collection_content)),
            (collection_id, TreeImpl.create(collection_content))
        ]

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.create(
                system_tree_it=[],
                node_tree_it=[],
                route_tree_it=[],
                collection_tree_it=collections_tree_items,
                environment_tree_it=[],
                entity_tree_it=[]
            )

        err_msg_template = (
            "Collections:\n"
            "  Collection [{}] already defined"
        )

        self.assertEqual(
            first=err_msg_template.format(
                collection_id
            ),
            second=str(err_ctx.exception)
        )


@skip("")
class ConfigurationCreateEnvironmentTest(TestCaseImpl):
    def test_create_environment_already_defined(self) -> None:
        environment_id = self.randomText()
        environment_content: object = {
            "zones": {}
        }

        environments_tree_items = [
            (environment_id, TreeImpl.create(environment_content)),
            (environment_id, TreeImpl.create(environment_content))
        ]

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.create(
                system_tree_it=[],
                node_tree_it=[],
                route_tree_it=[],
                collection_tree_it=[],
                environment_tree_it=environments_tree_items,
                entity_tree_it=[]
            )

        err_msg_template = (
            "Environments:\n"
            "  Environment [{}] already defined"
        )

        self.assertEqual(
            first=err_msg_template.format(
                environment_id
            ),
            second=str(err_ctx.exception)
        )


@skip("")
class ConfigurationCreateEntityTest(TestCaseImpl):
    def test_create_entity_already_defined(self) -> None:
        entity_id = self.randomText()

        entity_content: object = {
            "title": "",
            "wiki": "",
            "refs": [],
            "routes": []
        }
        entities_tree_items = [
            (entity_id, TreeImpl.create(entity_content)),
            (entity_id, TreeImpl.create(entity_content))
        ]

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            ConfigurationImpl.create(
                system_tree_it=[],
                node_tree_it=[],
                route_tree_it=[],
                collection_tree_it=[],
                environment_tree_it=[],
                entity_tree_it=entities_tree_items
            )

        err_msg_template = (
            "Entities:\n"
            "  Entity [{}] already defined"
        )

        self.assertEqual(
            first=err_msg_template.format(
                entity_id
            ),
            second=str(err_ctx.exception)
        )
