from unittest import skip

from databuscfg.processor.environment.impl import EnvironmentProcessorImpl
from databuscfg.validator.system.impl import SystemValidatorImpl
from pygentestcase.impl import TestCaseImpl
from pygentree.impl import TreeImpl
from pygentype.exception import ExpectedException


class EnvironmentProcessorTest(TestCaseImpl):
    @skip("")
    def test_process_producer_node_conflict(self) -> None:
        environment_id = self.randomText()
        zone1_id = "a" + self.randomText()
        zone2_id = "b" + self.randomText()
        vhost1_id = self.randomText()
        vhost2_id = self.randomText()
        producer_id = self.randomText()
        node_id = self.randomText()

        environment_tree = TreeImpl.create({
            "zones": {
                zone1_id: {
                    "vhosts": {
                        vhost1_id: {
                            "producers": {
                                producer_id: {
                                    "nodes": [node_id]
                                }

                            },
                            "consumers": {}
                        }
                    }
                },
                zone2_id: {
                    "vhosts": {
                        vhost2_id: {
                            "producers": {
                                producer_id: {
                                    "nodes": [node_id]
                                }

                            },
                            "consumers": {}
                        }
                    }
                }
            }
        })

        system_validator = SystemValidatorImpl(set([producer_id]))
        processor = EnvironmentProcessorImpl(system_validator)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(environment_id, environment_tree)

        err_msg_template = (
            "Environment [{}]:\n"
            "  Zone [{}]:\n"
            "    Vhost [{}]:\n"
            "      Producer [{}]:\n"
            "        Node [{}]:\n"
            "          Node already defined zones[{}].vhosts[{}]"
        )
        self.assertEqual(
            first=err_msg_template.format(
                environment_id,
                zone2_id,
                vhost2_id,
                producer_id,
                node_id,
                zone1_id,
                vhost1_id
            ),
            second=str(err_ctx.exception)
        )

    @skip("")
    def test_process_consumer_node_conflict(self) -> None:
        environment_id = self.randomText()
        zone1_id = "a" + self.randomText()
        zone2_id = "b" + self.randomText()
        vhost1_id = self.randomText()
        vhost2_id = self.randomText()
        consumer_id = self.randomText()
        node_id = self.randomText()

        environment_tree = TreeImpl.create({
            "zones": {
                zone1_id: {
                    "vhosts": {
                        vhost1_id: {
                            "producers": {},
                            "consumers": {
                                consumer_id: {
                                    "nodes": [node_id]
                                }
                            }
                        }
                    }
                },
                zone2_id: {
                    "vhosts": {
                        vhost2_id: {
                            "producers": {},
                            "consumers": {
                                consumer_id: {
                                    "nodes": [node_id]
                                }
                            }
                        }
                    }
                }
            }
        })

        system_validator = SystemValidatorImpl(set([consumer_id]))
        processor = EnvironmentProcessorImpl(system_validator)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(environment_id, environment_tree)

        err_msg_template = (
            "Environment [{}]:\n"
            "  Zone [{}]:\n"
            "    Vhost [{}]:\n"
            "      Consumer [{}]:\n"
            "        Node [{}]:\n"
            "          Node already defined zones[{}].vhosts[{}]"
        )
        self.assertEqual(
            first=err_msg_template.format(
                environment_id,
                zone2_id,
                vhost2_id,
                consumer_id,
                node_id,
                zone1_id, vhost1_id
            ),
            second=str(err_ctx.exception)
        )

    @skip("")
    def test_process_undefined_producer(self) -> None:
        environment_id = self.randomText()
        zone_id = self.randomText()
        vhost_id = self.randomText()
        producer_id = self.randomText()

        environment_tree = TreeImpl.create({
            "zones": {
                zone_id: {
                    "vhosts": {
                        vhost_id: {
                            "producers": {
                                producer_id: {
                                    "nodes": []
                                }

                            },
                            "consumers": {}
                        }
                    }
                }
            }
        })

        system_validator = SystemValidatorImpl(set([]))
        processor = EnvironmentProcessorImpl(system_validator)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(environment_id, environment_tree)

        err_msg_template = (
            "Environment [{}]:\n"
            "  Zone [{}]:\n"
            "    Vhost [{}]:\n"
            "      Producer [{}]:\n"
            "        System profile not found"
        )
        self.assertEqual(
            first=err_msg_template.format(
                environment_id,
                zone_id,
                vhost_id,
                producer_id
            ),
            second=str(err_ctx.exception)
        )

    @skip("")
    def test_process_undefined_consumer(self) -> None:
        environment_id = self.randomText()
        zone_id = self.randomText()
        vhost_id = self.randomText()
        consumer_id = self.randomText()

        environment_tree = TreeImpl.create({
            "zones": {
                zone_id: {
                    "vhosts": {
                        vhost_id: {
                            "producers": {},
                            "consumers": {
                                consumer_id: {
                                    "nodes": []
                                }
                            }
                        }
                    }
                }
            }
        })

        system_validator = SystemValidatorImpl(set([]))
        processor = EnvironmentProcessorImpl(system_validator)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(environment_id, environment_tree)

        err_msg_template = (
            "Environment [{}]:\n"
            "  Zone [{}]:\n"
            "    Vhost [{}]:\n"
            "      Consumer [{}]:\n"
            "        System profile not found"
        )
        self.assertEqual(
            first=err_msg_template.format(
                environment_id,
                zone_id,
                vhost_id,
                consumer_id
            ),
            second=str(err_ctx.exception)
        )

    @skip("")
    def test_process_node_correlation(self) -> None:
        environment_id = self.randomText()
        zone_id = self.randomText()
        vhost1_id = self.randomText()
        vhost2_id = self.randomText()
        system1_id = self.randomText()
        system2_id = self.randomText()
        node1_id = self.randomText()
        node2_id = self.randomText()

        environment_tree = TreeImpl.create({
            "zones": {
                zone_id: {
                    "vhosts": {
                        vhost1_id: {
                            "producers": {
                                system1_id: {
                                    "nodes": [node1_id]
                                }
                            },
                            "consumers": {
                                system2_id: {
                                    "nodes": [node2_id]
                                }
                            }
                        },
                        vhost2_id: {
                            "producers": {
                                system2_id: {
                                    "nodes": [node2_id]
                                }
                            },
                            "consumers": {
                                system1_id: {
                                    "nodes": [node1_id]
                                }
                            }
                        }
                    }
                }
            }
        })

        system_validator = SystemValidatorImpl(set([system1_id, system2_id]))
        processor = EnvironmentProcessorImpl(system_validator)

        processor.process(environment_id, environment_tree)
