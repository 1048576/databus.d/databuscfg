from unittest import skip

from databuscfg.processor.route.impl import RouteProcessorImpl
from databuscfg.validator.node.impl import NodeValidatorImpl
from databuscfg.validator.system.impl import SystemValidatorImpl
from pygentestcase.impl import TestCaseImpl
from pygentree.impl import TreeImpl
from pygentype.exception import ExpectedException


@skip("")
class RouteProcessorTest(TestCaseImpl):
    def test_process_undefined_producer_system(self) -> None:
        producer_id = self.randomText()
        route_id = self.randomText()

        route_tree = TreeImpl.create({
            "title": self.randomText(),
            "producer": producer_id,
            "consumer": "",
            "exclusive": False,
            "rules": []
        })

        system_validator = SystemValidatorImpl(set())
        node_validator = NodeValidatorImpl(set())
        processor = RouteProcessorImpl(
            system_validator=system_validator,
            node_validator=node_validator
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(route_id, route_tree)

        err_msg_template = (
            "Route [{}]:\n"
            "  Producer [{}]:\n"
            "    System profile not found"
        )

        self.assertEqual(
            first=err_msg_template.format(
                route_id,
                producer_id
            ),
            second=str(err_ctx.exception)
        )

    def test_process_undefined_consumer_system(self) -> None:
        producer_id = self.randomText()
        consumer_id = self.randomText()
        route_id = self.randomText()

        route_tree = TreeImpl.create({
            "title": self.randomText(),
            "producer": producer_id,
            "consumer": consumer_id,
            "exclusive": False,
            "rules": []
        })

        system_validator = SystemValidatorImpl(set([producer_id]))
        node_validator = NodeValidatorImpl(set())
        processor = RouteProcessorImpl(
            system_validator=system_validator,
            node_validator=node_validator
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(route_id, route_tree)

        err_msg_template = (
            "Route [{}]:\n"
            "  Consumer [{}]:\n"
            "    System profile not found"
        )

        self.assertEqual(
            first=err_msg_template.format(
                route_id,
                consumer_id
            ),
            second=str(err_ctx.exception)
        )

    def test_process_undefined_rule_producer(self) -> None:
        system_id = self.randomText()
        route_id = self.randomText()
        rule_producer_id = self.randomText()

        route_tree = TreeImpl.create({
            "title": self.randomText(),
            "producer": system_id,
            "consumer": system_id,
            "exclusive": False,
            "rules": [
                {
                    "producer": rule_producer_id,
                    "consumer": "",
                    "label": ""
                }
            ]
        })

        system_validator = SystemValidatorImpl(set([system_id]))
        node_validator = NodeValidatorImpl(set())
        processor = RouteProcessorImpl(
            system_validator=system_validator,
            node_validator=node_validator
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(route_id, route_tree)

        err_msg_template = (
            "Route [{}]:\n"
            "  Producer [{}]:\n"
            "    Node profile not found"
        )

        self.assertEqual(
            first=err_msg_template.format(
                route_id,
                rule_producer_id
            ),
            second=str(err_ctx.exception)
        )

    def test_process_undefined_rule_consumer(self) -> None:
        system_id = self.randomText()
        route_id = self.randomText()
        rule_producer_id = self.randomText()
        rule_consumer_id = self.randomText()

        route_tree = TreeImpl.create({
            "title": self.randomText(),
            "producer": system_id,
            "consumer": system_id,
            "exclusive": False,
            "rules": [
                {
                    "producer": rule_producer_id,
                    "consumer": rule_consumer_id,
                    "label": ""
                }
            ]
        })

        system_validator = SystemValidatorImpl(set([system_id]))
        node_validator = NodeValidatorImpl(set([rule_producer_id]))
        processor = RouteProcessorImpl(
            system_validator=system_validator,
            node_validator=node_validator
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(route_id, route_tree)

        err_msg_template = (
            "Route [{}]:\n"
            "  Consumer [{}]:\n"
            "    Node profile not found"
        )

        self.assertEqual(
            first=err_msg_template.format(
                route_id,
                rule_consumer_id
            ),
            second=str(err_ctx.exception)
        )

    def test_process_rule_correlation(self) -> None:
        system_id = self.randomText()
        route_id = self.randomText()
        rule_producer_id = self.randomText()
        rule_consumer_id = self.randomText()
        rule_label_id = self.randomText()

        route_tree = TreeImpl.create({
            "title": self.randomText(),
            "producer": system_id,
            "consumer": system_id,
            "exclusive": False,
            "rules": [
                {
                    "producer": rule_producer_id,
                    "consumer": rule_consumer_id,
                    "label": rule_label_id
                },
                {
                    "producer": "",
                    "consumer": rule_consumer_id,
                    "label": rule_label_id
                },
                {
                    "producer": rule_producer_id,
                    "consumer": "",
                    "label": rule_label_id
                },
                {
                    "producer": rule_producer_id,
                    "consumer": rule_consumer_id,
                    "label": ""
                }
            ]
        })

        system_validator = SystemValidatorImpl(set([system_id]))
        node_validator = NodeValidatorImpl(
            nodes=set([rule_producer_id, rule_consumer_id, ""])
        )
        processor = RouteProcessorImpl(
            system_validator=system_validator,
            node_validator=node_validator
        )

        processor.process(route_id, route_tree)

    def test_process_rule_already_defined(self) -> None:
        system_id = self.randomText()
        route_id = self.randomText()
        rule_producer_id = self.randomText()
        rule_consumer_id = self.randomText()
        rule_label_id = self.randomText()

        route_tree = TreeImpl.create({
            "title": self.randomText(),
            "producer": system_id,
            "consumer": system_id,
            "exclusive": False,
            "rules": [
                {
                    "producer": rule_producer_id,
                    "consumer": rule_consumer_id,
                    "label": rule_label_id
                },
                {
                    "producer": rule_producer_id,
                    "consumer": rule_consumer_id,
                    "label": rule_label_id
                }
            ]
        })

        system_validator = SystemValidatorImpl(set([system_id]))
        node_validator = NodeValidatorImpl(
            nodes=set([rule_producer_id, rule_consumer_id])
        )
        processor = RouteProcessorImpl(
            system_validator=system_validator,
            node_validator=node_validator
        )

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(route_id, route_tree)

        err_msg_template = (
            "Route [{}]:\n"
            "  Rule [{}]->[{}]:[{}] already defined"
        )

        self.assertEqual(
            first=err_msg_template.format(
                route_id,
                rule_producer_id,
                rule_consumer_id,
                rule_label_id
            ),
            second=str(err_ctx.exception)
        )
