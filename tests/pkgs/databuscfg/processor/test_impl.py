from databuscfg.processor.abstract import Item
from databuscfg.processor.abstract import ItemProcessor
from databuscfg.processor.exception import ItemAlreadyDefinedException
from databuscfg.processor.impl import ItemsProcessorImpl
from pygentestcase.impl import TestCaseImpl
from pygentree.abstract import Tree


class ItemMock(Item):
    ...


class ItemProcessorMock(ItemProcessor[ItemMock]):
    def process(self, unique_id: str, tree: Tree) -> ItemMock:
        return ItemMock()


class ItemsProcessorTest(TestCaseImpl):
    def test_process_item_already_defined(self) -> None:
        collection_name = self.randomText()
        item_name = self.randomText()
        item_id = self.randomText()

        processor = ItemsProcessorImpl(
            collection_name=collection_name,
            item_name=item_name,
            item_processor=ItemProcessorMock()
        )

        err_ctx = self.assertRaises(ItemAlreadyDefinedException)
        with (err_ctx):
            processor.process(
                items=[
                    (item_id, Tree()),
                    (item_id, Tree())
                ]
            )

        self.assertEqual(
            first=ItemAlreadyDefinedException(
                msg="{}:\n  {} [{}] already defined".format(
                    collection_name, item_name, item_id
                )
            ),
            second=err_ctx.exception
        )
