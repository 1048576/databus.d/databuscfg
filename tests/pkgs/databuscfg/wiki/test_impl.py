import shutil

from unittest import TestCase

from databuscfg.configuration.impl import ConfigurationImpl
from databuscfg.loader.impl import LoaderImpl
from databuscfg.uploader.impl import UploaderImpl
from databuscfg.wiki.impl import WikiImpl
from pygenpath.impl import PathResolverImpl
from pygentestcase.file.impl import FileTestCaseImpl


class WikiTest(TestCase, FileTestCaseImpl):
    def test_generate(self) -> None:
        configuration_path_resolver = PathResolverImpl.create(
            workdir="./samples/databuscfg/configuration/"
        )
        wiki_path_resolver_expected = PathResolverImpl.create(
            workdir="./samples/databuscfg/wiki/"
        )

        wiki_path_resolver_actual = PathResolverImpl.temp()
        try:
            loader = LoaderImpl(configuration_path_resolver)
            wiki_uploader = UploaderImpl.create(
                resolver=wiki_path_resolver_actual
            )

            configuration = ConfigurationImpl.load(loader)

            wiki = WikiImpl.create()

            wiki.generate(configuration, wiki_uploader)

            self.assertDirContentEqual(
                expected_path_resolver=wiki_path_resolver_expected,
                actual_path_resolver=wiki_path_resolver_actual
            )
        finally:
            shutil.rmtree(wiki_path_resolver_actual.pwd())
